import React, { Component } from 'react';
import axios from 'axios';

import Input from '../../ui/input';
import Button from '../../ui/button';

import { config } from '../../config';

class Login extends Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      email: '',
      password: ''
    }
  }
  
  componentDidMount() {
    
  }
  
  handleChange(e, data) {
    console.log(e.target.name)
    console.log(e.target.value)
    let property = e.target.name;
    this.setState({
      property : e.target.value 
    });
  }
  
  handleSubmit() {
    let loginParams = {
      email: this.state.email,
      password: this.state.password
    }
    console.log(config.baseURL);
    axios.post(config.baseURL + 'auth/login/', loginParams)
      .then((response) => {
          alert(response.data);
      })
      .catch((err) => {
        console.log(err);
      })
  }
  
  render() {
    return(
      <div className="login-form">
        <Input 
          name={"email"}
          type={"text"}
          placeholder={"Your email"}
          changeHandler={this.handleChange} 
        />
        <Input 
          name={"password"}
          type={"password"}
          placeholder={"Your password"}
          changeHandler={this.handleChange} 
        />
        <Button 
          buttonText={"Submit"}
          clickHandler={this.handleSubmit}
          variant={'info'}
        /> 
      </div>
    )
  }
}

export default Login;