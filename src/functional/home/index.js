import React, { Component } from 'react';
import axios from 'axios';

import Card from '../../ui/card';
import Spinner from '../../ui/spinner';
import { config } from '../../config';

import '../../home.css';

class Home extends Component {
  constructor() {
    super();
    this.state = {
      places: [],
      loading: true
    }
  }

  componentDidMount() {
    axios.get(config.baseURL + 'places')
      .then((response) => {
        this.setState({
          places: response.data,
          loading: false
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }
  
  renderPlaceDetails(key) {
    console.log('hey now : ' + key)
  }
  
  renderCards() {
    if(this.state.loading) {
      return(
        <Spinner />
      )
    } else {
      return(
        this.state.places.map((place) => {
          return(
            <Card key={place.restaurant.id} onClick={() => this.renderPlaceDetails(place.restaurant.id)}>
              <div className="card-body">
                <nav className="level">
                  <div className="level-left left-box">
                    <div className="heading-name">
                      <h3 className="subtitle is-3">
                        {place.restaurant.name}
                      </h3>
                    </div>
                    <div className="locality-subHeading">
                      <h5 className="subtitle is-5">
                        {place.restaurant.location.locality_verbose}
                      </h5>
                    </div>
                  </div>
                  <div className="level-right thumbnail-container">
                    <img src={place.restaurant.thumb} alt={place.restaurant.name} />
                  </div>
                </nav>
              </div>
            </Card>
          )
        })
      )
    }
  }

  render() {
    return(
      <div className="home">
        {this.renderCards()}
      </div>
    )
  }
}

export default Home;