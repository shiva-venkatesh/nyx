import React, { Component } from 'react';
import {
  Link,
} from 'react-router-dom';

class Navbar extends Component {
  render() {
    return(
      <div className="navbar">
        <nav className="nav-icon-container">
          <Link to="/home">
            <div className="nav-icons">
              <i className="fa fa-2x fa-home" aria-hidden="true"></i>
            </div>
          </Link>
          <Link to="/feed">
            <div className="nav-icons">
              <i className="fa fa-2x fa-newspaper-o" aria-hidden="true"></i>
            </div>
          </Link>
          <Link to="/social">
            <div className="nav-icons">
              <i className="fa fa-2x fa-share-square-o" aria-hidden="true"></i>
            </div>
          </Link>
        </nav>
      </div>
    )
  }
}

export default Navbar;