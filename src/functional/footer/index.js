import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return(
      <footer className="footer">
        <div className="container">
            <h5 className="subtitle is-5">
              Created by <strong>Shiva Venkatesh</strong>
            </h5>
        </div>
      </footer>      
    )
  }
}

export default Footer;