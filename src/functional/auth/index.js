import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import Login from '../login';
import Register from '../register';

import '../../auth.css';

class Auth extends Component {
  constructor() {
    super()
    this.state = {
      activeTab: "Login"
    }
  }
  
  render() {
    return(
      <div className="auth-container">   
        <h1 className="title is-1">
          Auth
        </h1>
        <div className="auth-tabs">
          <Tabs>
            <TabList>
              <Tab>
                Login
              </Tab>
              <Tab>
                Register
              </Tab>
            </TabList>
            <TabPanel>
              <Login />
            </TabPanel>
            <TabPanel>
              <Register />
            </TabPanel>
          </Tabs>
        </div>               
      </div>
    )
  }
}

export default Auth;