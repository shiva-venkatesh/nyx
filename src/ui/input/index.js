import React, { Component } from 'react';

import '../../input.css';

class Input extends Component {
  constructor() {
    super()
  }
    
  componentDidMount() {
    
  }
  
  render() {
    return(
      <div className="input-container">
        <input className="input" name={this.props.name} type={this.props.type} placeholder={this.props.placeholder} onChange={this.props.changeHandler} />
      </div>
    )
  }
}

export default Input;