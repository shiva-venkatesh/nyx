import React from 'react';

import Button from '../button';

const Pill = ({leftTitle, rightTitle}) => {
    return(
        <div className="pill-box level">
            <h5 className="pill-text level-left">
                {'value : ' + leftTitle}
            </h5>
            <div>
                 | 
            </div>
            <h5 className="pill-text level-right">
                {'updated at : ' + rightTitle}
            </h5>
        </div>
    )
}

export default Pill;