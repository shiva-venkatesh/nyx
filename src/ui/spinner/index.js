import React, { Component } from 'react';

class Spinner extends Component {
  render() {
    return(
      <div className="spinner-container">
        <i className="fa fa-4x fa-spin fa-spinner" aria-hidden="true"></i>
      </div>
    )
  }
}

export default Spinner;