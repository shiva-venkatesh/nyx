import React, { Component } from 'react';
import {
  Route
} from 'react-router-dom';

import Heading from './ui/header';

import Home from './functional/home';
import Feed from './functional/feed';
import Social from './functional/social';
import Navbar from './functional/navbar';
import Footer from './functional/footer';
import Auth from './functional/auth';

import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      authenticated: false
    }
  }
  
  componentDidMount() {
    let authenticatedState = localStorage.getItem('nyxToken') ? true : false;
    console.log(authenticatedState)
    this.setState({
      authenticated : authenticatedState
    });
  }
  
  renderAuthScreens() {
    return(
      <div className="app-body">
        <Auth />
      </div>
    )
  }
  
  renderApp() {
    return(
      <div className="app-body">
        <div className="app-container container">
          <Heading
            headerText={'Nyx'}
            classes={['brand-header']}
          />
          <Navbar />
          <div>
            <Route path="/home" component={Home}/>
            <Route path="/feed" component={Feed}/>
            <Route path="/social" component={Social}/>
          </div>
        </div>
        <Footer />
      </div>
    )
  }
  
  render() {
    return (
      <div className="App">
        { this.state.authenticated ? this.renderApp() : this.renderAuthScreens() }
      </div>
    );
  }
}

export default App;
